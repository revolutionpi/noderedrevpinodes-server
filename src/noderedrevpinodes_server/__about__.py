# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH
# SPDX-License-Identifier: GPL-2.0-or-later
"""Metadata of package."""
__author__ = "erminas GmbH, KUNBUS GmbH"
__copyright__ = "Copyright (C) 2019-2023 erminas GmbH, 2025 KUNBUS GmbH"
__license__ = "LGPL-3.0-only"
__version__ = "1.2.0"
