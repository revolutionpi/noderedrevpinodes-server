# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2024 KUNBUS GmbH
# SPDX-License-Identifier: GPL-2.0-or-later
"""Package: noderedrevpinodes-server."""
from .__about__ import __author__, __copyright__, __license__, __version__
